package com.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class API_SendPost
{

	DefaultHttpClient httpClient;
	HttpContext localContext;
	private String result;

	HttpResponse response = null;
	HttpPost httpPost = null;
	HttpGet httpGet = null;

	public API_SendPost()
	{

		HttpParams myParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(myParams, 10000);
		HttpConnectionParams.setSoTimeout(myParams, 10000);
		httpClient = new DefaultHttpClient(myParams);
		localContext = new BasicHttpContext();
	}

	public void clearCookies()
	{
		httpClient.getCookieStore().clear();
	}

	public void abort()
	{
		try
		{
			if (httpClient != null)
			{
				System.out.println("Abort.");
				httpPost.abort();
			}
		}
		catch (Exception e)
		{
			System.out.println("abort" + e);
		}
	}

	public String sendPost(String url, String data)
	{
		return sendPost(url, data, null);
	}

	public String sendJSONPost(String url, JSONObject data)
	{
		return sendPost(url, data.toString(), "application/json");
	}

	public String sendPost(String url, String data, String contentType)
	{
		result = null;

		httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2109);

		httpPost = new HttpPost(url);
		response = null;

		StringEntity tmp = null;

		httpPost.setHeader("User-Agent", "SET YOUR USER AGENT STRING HERE");
		httpPost.setHeader("Accept", "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");

		if (contentType != null)
		{
			httpPost.setHeader("Content-Type", contentType);
		}
		else
		{
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
		}

		try
		{
			tmp = new StringEntity(data, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			System.out.println("Encode Exception" + e);
		}

		httpPost.setEntity(tmp);

		try
		{
			response = httpClient.execute(httpPost, localContext);

			if (response != null)
			{
				result = EntityUtils.toString(response.getEntity());
			}
		}
		catch (Exception e)
		{
			System.out.println("Error in sendPost" + e);
		}

		return result;
	}

	public String sendGet(String url)
	{
		httpGet = new HttpGet(url);

		try
		{
			response = httpClient.execute(httpGet);
		}
		catch (Exception e)
		{
			Log.e("Your App Name Here", e.getMessage());
		}

		try
		{
			result = EntityUtils.toString(response.getEntity());
		}
		catch (IOException e)
		{
			Log.e("Your App Name Here", e.getMessage());
		}

		return result;
	}

	public JSONObject setJsonData(Context c, int key, String token, int sex, int age, int weight, int height, String name, String work)
	{

		JSONObject jsData = new JSONObject();
		JSONObject userdata = new JSONObject();
		try
		{
			//這裡放json格式
			jsData.put("apikey", key);
			jsData.put("userdata", userdata.put("user_fb_token", token));
			jsData.put("userdata", userdata.put("user_sex", sex));
			jsData.put("userdata", userdata.put("user_age", age));
			jsData.put("userdata", userdata.put("user_weight", weight));
			jsData.put("userdata", userdata.put("user_height", height));
			jsData.put("userdata", userdata.put("user_name", name));
			jsData.put("userdata", userdata.put("user_work_intensity", work));

			// jsData.put("user_fb_token", token);
			// jsData.put("user_sex", sex);
			// jsData.put("user_age", age);
			// jsData.put("user_weight", weight);
			// jsData.put("user_height", height);
			// jsData.put("user_name", name);
			// jsData.put("user_work_intensity", work);
			Log.e("Json Data", "Json Data For Home is  : " + jsData.toString());

		}
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		return jsData;
	}

	public JSONObject getJSONreturn(String tmp) throws JSONException
	{
		String msg = tmp;
		JSONObject content = new JSONObject(msg);
		return content;
	}

	// public JSONObject getJSONData(String url){
	// HttpGet httpget = new HttpGet(url);
	// try{
	// HttpResponse httprspns = gethttpclient().execute(httpget);
	// String retsrc = EntityUtils.toString(httprspns.getEntity());
	// Log.e("retsrc",retsrc);
	// JSONObject content = new JSONObject(retsrc);
	// return content;
	// }catch(Exception e){
	// e.printStackTrace();
	// return null;
	// }
	// }
	//
	// private DefaultHttpClient gethttpclient(){
	// HttpParams httpPara = new BasicHttpParams();
	// int timeoutConnection = 5000;
	// HttpConnectionParams.setConnectionTimeout(httpPara, timeoutConnection);
	// int timeoutSocket = 3000;
	// HttpConnectionParams.setSoTimeout(httpPara, timeoutSocket);
	// DefaultHttpClient httpclinet = new DefaultHttpClient(httpPara);
	// return httpClient;
	// }
}
